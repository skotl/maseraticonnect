﻿namespace Kwolo.MaseratiConnect.TestHarness.Config;

public class MaseratiConnectOptions
{
    public const string Key = "maseratiConnect";

    public string LoginUrl { get; set; } = null!;
    public string DashboardUrl { get; set; } = null!;
    public string UserLogin { get; set; } = null!;
    public string UserPassword { get; set; } = null!;
}
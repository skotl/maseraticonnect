﻿using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.TestHarness.Config;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Kwolo.MaseratiConnect.TestHarness.Worker;

public class WorkerService : BackgroundService
{
    private readonly IHostApplicationLifetime _hostApplicationLifetime;
    private readonly ILogger<WorkerService> _logger;
    private readonly IMaseratiConnector _maseratiConnector;
    private readonly MaseratiConnectOptions _options;

    public WorkerService(
        IHostApplicationLifetime hostApplicationLifetime,
        ILogger<WorkerService> logger,
        IMaseratiConnector maseratiConnector,
        IOptions<MaseratiConnectOptions> options)
    {
        _hostApplicationLifetime = hostApplicationLifetime;
        _logger = logger;
        _maseratiConnector = maseratiConnector;
        _options = options.Value;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Worker started");

        var car = await _maseratiConnector.FetchDataAsync(From(_options));

        _logger.LogInformation("Got a car with VIN={VIN} and {Wheels} wheels!", car?.Vin, car?.WheelCount);

        _logger.LogInformation("Worker ending...");
        _hostApplicationLifetime.StopApplication();
    }

    private static ConnectionDetails From(MaseratiConnectOptions options)
    {
        return new ConnectionDetails
        {
            LoginUrl = options.LoginUrl,
            DashboardUrl = options.DashboardUrl,
            UserLogin = options.UserLogin,
            UserPassword = options.UserPassword,
            ShowChromiumWindow = false
        };
    }
}
﻿using Kwolo.MaseratiConnect;
using Kwolo.MaseratiConnect.TestHarness.Config;
using Kwolo.MaseratiConnect.TestHarness.Worker;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var builder = Host.CreateApplicationBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json", optional: false);
builder.Services.AddOptions<MaseratiConnectOptions>()
    .Bind(builder.Configuration.GetSection(MaseratiConnectOptions.Key));

// Add Maserati Connect services into the collection
builder.Services.UseMaseratiConnect();

// The test code - gets data from Maserati Connect
builder.Services.AddHostedService<WorkerService>();

using var host = builder.Build();

await host.RunAsync();

﻿using System.Text;
using System.Text.Json;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Tests.JsonConverters;

public static class JavascriptNullToDecimalConverterTests
{
    [Theory]
    [InlineData("\"23\"", 23.0)]
    [InlineData("\"null\"", null)]
    [InlineData("null", null)]
    public static void Test(string json, double? result)
    {
        var expectedResultAsDecimal = (decimal?)result; // Issues passing decimals to test parameters

        var jsonReader = new Utf8JsonReader(
            Encoding.UTF8.GetBytes(json),
            false,
            new JsonReaderState(new JsonReaderOptions{}));
        jsonReader.Read();

        var converted = new JavascriptNullToDecimalConverter().Read(ref jsonReader, typeof(decimal?), new JsonSerializerOptions());

        Assert.Equal(expectedResultAsDecimal, converted);
    }
}
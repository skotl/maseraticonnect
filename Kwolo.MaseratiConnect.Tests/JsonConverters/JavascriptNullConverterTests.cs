﻿using System.Text;
using System.Text.Json;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Tests.JsonConverters;

public class JavascriptNullConverterTests
{
    [Theory]
    [InlineData("\"hello\"", "hello")]
    [InlineData("\"null\"", null)]
    [InlineData("null", null)]
    public static void Test(string json, string? result)
    {
        var jsonReader = new Utf8JsonReader(
            Encoding.UTF8.GetBytes(json),
            false,
            new JsonReaderState(new JsonReaderOptions{}));
        jsonReader.Read();

        var converted = new JavascriptNullConverter().Read(ref jsonReader, typeof(decimal?), new JsonSerializerOptions());

        Assert.Equal(result, converted);
    }
}
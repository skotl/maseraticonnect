﻿using System.Data;
using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public class FuelConverterTests
{
    private const string RawJsonSparseStatus = """
                                               {
                                                   "activationTimestamp": 0,
                                                   "svlaStatus": false
                                               }
                                               """;

    private const string RawJsonGoodStatus = """
                                   {
                                   "vehicleInfo": {
                                       "batteryInfo": {
                                           "batteryStateOfCharge": "null",
                                           "batteryStatus": "0",
                                           "batteryVoltage": {
                                               "unit": "volts",
                                               "value": "14.3"
                                           }
                                       },
                                       "fuel": {
                                           "distanceToEmpty": {
                                               "unit": "km",
                                               "value": "33"
                                           },
                                           "fuelAmount": {
                                               "unit": "l",
                                               "value": "30.0"
                                           },
                                           "fuelAmountLevel": 36,
                                           "isFuelLevelLow": true
                                       }
                                     }
                                   }
                                   """;
    [Fact]
    public static void CanConvert()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus)
        };

        var fuel = FuelConverter.Convert(captures);
        Assert.NotNull(fuel);
        Assert.Equal(36M, fuel.FuelPercentageFull);
        Assert.Equal(30M, fuel.FuelAmount);
        Assert.Equal("l", fuel.FuelAmountUnits);
        Assert.Equal(20M, fuel.DistanceToEmptyMiles);
    }

    [Fact]
    public static void ThrowsOnMultipleBatteryResponses()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus),
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus)
        };

        Assert.Throws<DataException>(() => FuelConverter.Convert(captures));
    }

    /// <summary>
    /// Some requests to the API endpoint will return a sparse JSON struct, so make sure we ignore that
    /// </summary>
    [Fact]
    public static void ReturnsNullForInvalidJson()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonSparseStatus)
        };

        var fuel = FuelConverter.Convert(captures);
        Assert.Null(fuel);
    }
}
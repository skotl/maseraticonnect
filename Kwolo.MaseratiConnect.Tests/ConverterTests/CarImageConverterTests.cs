﻿using System.Text.Json;
using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Tests.Helpers;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class CarImageConverterTests
{
    [Fact]
    public static void CanConvertSingleCar()
    {
        var body = new
        {
            items = new[]
            {
                new
                {
                    id = "string",
                    preciseImageURL =
                        "https://gcvws.maserati.com/mws/connvehicle/api/photocopierByVIN?view=mainExtFront&scaleX=260&scaleY=160&scenarioKey=transparent&source=GCV&vin=ZN6XU61C00X404619&vinhash=ba0b2370e6a0e909f90107e42341fcf4a8bd0ea0"
                }
            }
        };

        var imageBodyList = new List<CaptureBody>
        {
            CaptureBodyTestHelper.ToImageBody(body)
        };

        var convertedList = CarImageConverter.Convert(imageBodyList).ToList();

        Assert.NotNull(convertedList);
        Assert.Single(convertedList);
        Assert.Equal(260, convertedList[0].Width);
        Assert.Equal(160, convertedList[0].Height);
        Assert.Equal("mainExtFront", convertedList[0].View);
        Assert.NotEmpty(convertedList[0].Url);
    }

    [Fact]
    public static void CanConvertMultipleEntries()
    {
        var body1 = new
        {
            items = new[]
            {
                new { id = "string", preciseImageURL = "https://item1" },
                new { id = "string", preciseImageURL = "https://item2" },
            }
        };
        var body2 = new
        {
            items = new[]
            {
                new { id = "string", preciseImageURL = "https://item3" },
                new { id = "string", preciseImageURL = "https://item4" },
            }
        };

        var imageBodyList = new List<CaptureBody>
        {
            CaptureBodyTestHelper.ToBody(new object(), CaptureFilters.FilterTypes.Location),
            CaptureBodyTestHelper.ToImageBody(body1),
            CaptureBodyTestHelper.ToBody(new object(), CaptureFilters.FilterTypes.VehicleStatus),
            CaptureBodyTestHelper.ToImageBody(body2),
        };

        var convertedList = CarImageConverter.Convert(imageBodyList).ToList();

        Assert.NotNull(convertedList);
        Assert.Equal(4, convertedList.Count);
        Assert.EndsWith("item1", convertedList[0]?.Url);
        Assert.EndsWith("item2", convertedList[1]?.Url);
        Assert.EndsWith("item3", convertedList[2]?.Url);
        Assert.EndsWith("item4", convertedList[3]?.Url);
    }

    [Fact]
    public static void ThrowsOnNullCaptures()
    {
        Assert.Throws<ArgumentNullException>(() => CarImageConverter.Convert(null));
    }
}
﻿using System.Data;
using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public class LocationConverterTests
{
    private const string RawJsonGoodStatus = """
                                             {
                                                 "altitude": 55,
                                                 "bearing": 12,
                                                 "isLocationApprox": true,
                                                 "latitude": 55.9862670898438,
                                                 "longitude": -3.38045310974121,
                                                 "timeStamp": 1700562213592
                                             }
                                             """;

    [Fact]
    public static void CanConvert()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.Location, RawJsonGoodStatus)
        };

        var location = LocationConverter.Convert(captures);
        Assert.NotNull(location);
        Assert.Equal(55, location.Altitude);
        Assert.Equal(12, location.Bearing);
        Assert.Equal(55.9862670898438, location.Latitude);
        Assert.Equal(-3.38045310974121, location.Longitude);
    }

    [Fact]
    public static void ThrowsOnMultipleLocationResponses()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.Location, RawJsonGoodStatus),
            new CaptureBody(CaptureFilters.FilterTypes.Location, RawJsonGoodStatus)
        };

        Assert.Throws<DataException>(() => LocationConverter.Convert(captures));
    }
}
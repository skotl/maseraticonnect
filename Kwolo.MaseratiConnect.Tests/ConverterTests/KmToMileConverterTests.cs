﻿using Kwolo.MaseratiConnect.Converters;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class KmToMileConverterTests
{
    [Fact]
    public static void CanConvertDouble()
    {
        Assert.Equal(62d, KmToMileConverter.KmToMileInt(100d));
    }

    [Fact]
    public static void CanConvertNullableDecimal()
    {
        decimal? v = 100;
        Assert.Equal(62, KmToMileConverter.KmToMileInt(v));
    }

    [Fact]
    public static void NullableDecimalCanReturn0()
    {
        decimal? v = null;
        Assert.Equal(0, KmToMileConverter.KmToMileInt(v));
    }

}
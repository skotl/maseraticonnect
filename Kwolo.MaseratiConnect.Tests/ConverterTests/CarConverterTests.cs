﻿using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class CarConverterTests
{
    [Fact]
    public static void CanConvert()
    {
        var dtoStructure = new ReportCardDto
        {
            LastVehicleUpdate = new DateTime(1990, 1, 1),
            Fetched = new DateTime(2000, 1, 1),
            Vin = "my-car-vin",
            WheelCount = 4,
            ItemsLists = new List<ItemValueDto>
            {
                new() { ItemKey = "odometer", Value = "5000 " },
                new()
                {
                    ItemKey = "wheelsAndSteering", Items = new List<ItemValueDto>
                    {
                        new() { ItemKey = "tirePressureLF", Value = "10" },
                        new() { ItemKey = "tirePressureRF", Value = "20" },
                        new() { ItemKey = "tirePressureLR", Value = "30" },
                        new() { ItemKey = "tirePressureRR", Value = "40" }
                    }
                },
                new() { ItemKey = "distanceToService", Value = "1000" },
                new() { ItemKey = "daysToService", Value = "365" },
                new() { ItemKey = "isServiceNeeded", Value = "true" },
                new()
                {
                    ItemKey = "vehicleStatus", Items = new()
                    {
                        new() { ItemKey = "rearBulb", Value = "50%" },
                        new() { ItemKey = "frontBulb", Value = "0" },
                        new() { ItemKey = "exhaust", Value = "smelly" }
                    }
                }
            }
        };

        var car = CarConverter.Convert(dtoStructure);
        Assert.NotNull(car);

        Assert.Equal(new DateTime(1990, 1, 1), car.LastVehicleUpdate);
        Assert.Equal(new DateTime(2000, 1, 1), car.TimeStamp);
        Assert.Equal("my-car-vin", car.Vin);
        Assert.Equal(4, car.WheelCount);
        Assert.Equal(3106, car.OdometerMiles);
        Assert.Equal(10, car.TyrePressures.LeftFront);
        Assert.Equal(20, car.TyrePressures.RightFront);
        Assert.Equal(30, car.TyrePressures.LeftRear);
        Assert.Equal(40, car.TyrePressures.RightRear);
        Assert.Equal(1000, car.DistanceToService);
        Assert.Equal(365, car.DaysToService);
        Assert.True(car.IsServiceNeeded);
        Assert.Equal(2, car.ServiceItemStatusList.Count);
        Assert.Equal("50%", car.ServiceItemStatusList[0].Value);
        Assert.Equal("smelly", car.ServiceItemStatusList[1].Value);
    }
}
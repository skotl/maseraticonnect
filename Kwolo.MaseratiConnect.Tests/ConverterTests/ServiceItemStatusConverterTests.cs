﻿using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class ServiceItemStatusConverterTests
{
    [Fact]
    public static void CanConvert()
    {
        var dtoStructure = new List<ItemValueDto>
        {
            new ()
            {
                ItemKey = "vehicleStatus", Items = new List<ItemValueDto>
                {
                    new() { ItemKey = "item1", Value = null },
                    new() { ItemKey = "item2", Value = "10" },
                    new() { ItemKey = "item3", Value = "20" },
                    new() { ItemKey = "item4", Value = "0" }
                }
            }
        };

        var serviceItems = ServiceItemStatusConverter.Convert(dtoStructure);
        Assert.NotNull(serviceItems);
        Assert.Equal(2, serviceItems.Count);
        Assert.Equal("item2", serviceItems[0].Item);
        Assert.Equal("item3", serviceItems[1].Item);
        Assert.Equal("10", serviceItems[0].Value);
        Assert.Equal("20", serviceItems[1].Value);
    }
}
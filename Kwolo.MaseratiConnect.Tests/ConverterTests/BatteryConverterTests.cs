﻿using System.Data;
using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class BatteryAndFuelConverterTests
{
    private const string RawJsonSparseStatus = """
                                               {
                                                   "activationTimestamp": 0,
                                                   "svlaStatus": false
                                               }
                                               """;

    private const string RawJsonGoodStatus = """
                                   {
                                   "vehicleInfo": {
                                       "batteryInfo": {
                                           "batteryStateOfCharge": "good",
                                           "batteryStatus": "3",
                                           "batteryVoltage": {
                                               "unit": "volts",
                                               "value": "14.3"
                                           }
                                       },
                                       "fuel": {
                                           "distanceToEmpty": {
                                               "unit": "km",
                                               "value": "null"
                                           },
                                           "fuelAmount": {
                                               "unit": "l",
                                               "value": "30.0"
                                           },
                                           "fuelAmountLevel": 36,
                                           "isFuelLevelLow": false
                                       }
                                     }
                                   }
                                   """;
    [Fact]
    public static void CanConvert()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus)
        };

        var battery = BatteryConverter.Convert(captures);
        Assert.NotNull(battery);
        Assert.Equal("good", battery.StateOfCharge);
        Assert.Equal(3, battery.Status);
        Assert.Equal(14.3M, battery.Voltage);
    }

    [Fact]
    public static void ThrowsOnMultipleBatteryResponses()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus),
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonGoodStatus)
        };

        Assert.Throws<DataException>(() => BatteryConverter.Convert(captures));
    }

    /// <summary>
    /// Some requests to the API endpoint will return a sparse JSON struct, so make sure we ignore that
    /// </summary>
    [Fact]
    public static void ReturnsNullForInvalidJson()
    {
        var captures = new List<CaptureBody>
        {
            new CaptureBody(CaptureFilters.FilterTypes.BatteryAndFuel, RawJsonSparseStatus)
        };

        var battery = BatteryConverter.Convert(captures);
        Assert.Null(battery);
    }
}
﻿using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class TyrePressureConverterTests
{
    [Fact]
    public static void CanConvert()
    {
        var dtoStructure = new List<ItemValueDto>
        {
            new ()
            {
                ItemKey = "wheelsAndSteering", Items = new List<ItemValueDto>
                {
                    new() { ItemKey = "tirePressureLF", Value = "5" },
                    new() { ItemKey = "tirePressureRF", Value = "10" },
                    new() { ItemKey = "tirePressureLR", Value = "15" },
                    new() { ItemKey = "tirePressureRR", Value = "20" }
                }
            }
        };

        var tyres = TyrePressureConverter.Convert(dtoStructure);
        Assert.NotNull(tyres);

        Assert.Equal(5, tyres.LeftFront);
        Assert.Equal(10, tyres.RightFront);
        Assert.Equal(15, tyres.LeftRear);
        Assert.Equal(20, tyres.RightRear);
    }
}
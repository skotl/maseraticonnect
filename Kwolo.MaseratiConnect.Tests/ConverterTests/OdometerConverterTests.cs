﻿using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class OdometerConverterTests
{
    [Fact]
    public static void CanConvert()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "odometer", Value = "5000" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        var odometer = OdometerConverter.Convert(items);
        Assert.NotNull(odometer);
        Assert.Equal(3106, odometer);
    }
}
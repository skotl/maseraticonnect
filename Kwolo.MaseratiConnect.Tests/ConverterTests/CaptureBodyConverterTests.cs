﻿using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Tests.Helpers;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class CaptureBodyConverterTests
{
    #region Testing classes
    internal class TestDto
    {
        [JsonPropertyName("first")]
        public string FirstProp { get; set; }
        [JsonPropertyName("second")]
        public int Number { get; set; }
    }
    #endregion

    [Fact]
    public static void CanConvert()
    {
        var testObject = new
        {
            first = "hello",
            second = 42
        };

        var dto = new List<CaptureBody>
        {
            CaptureBodyTestHelper.ToImageBody(testObject)
        }
            .ConvertToDto<TestDto>(CaptureFilters.FilterTypes.Image)
            .ToList();

        Assert.NotNull(dto);
        Assert.Single(dto);
        Assert.Equal("hello", dto[0]?.FirstProp);
        Assert.Equal(42, dto[0]?.Number);
    }

    [Fact]
    public static void CanFilter()
    {
        var testObject = new
        {
            first = "hello",
            second = 42
        };

        var dto = new List<CaptureBody>
            {
                CaptureBodyTestHelper.ToBody(testObject, CaptureFilters.FilterTypes.VehicleStatus),
                CaptureBodyTestHelper.ToBody(testObject, CaptureFilters.FilterTypes.Image),
                CaptureBodyTestHelper.ToBody(testObject, CaptureFilters.FilterTypes.Location)
            }
            .ConvertToDto<TestDto>(CaptureFilters.FilterTypes.Image)
            .ToList();

        Assert.NotNull(dto);
        Assert.Single(dto);
        Assert.Equal("hello", dto[0]?.FirstProp);
        Assert.Equal(42, dto[0]?.Number);
    }
    
}
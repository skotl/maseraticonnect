﻿using Kwolo.MaseratiConnect.Converters;
using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Tests.ConverterTests;

public static class ValueGetterTests
{
    [Fact]
    public static void CanGetForKey()
    {
        var items = new List<ItemValueDto>
        {
            new ItemValueDto { ItemKey = "aaa", Value = "111" },
            new ItemValueDto { ItemKey = "bbb", Value = "222" },
            new ItemValueDto { ItemKey = "ccc", Value = "333" }
        };

        Assert.Equal("222", items.GetForKey("bbb")?.Value);
    }

    [Fact]
    public static void GetReturnsNullOnMissingKey()
    {
        var items = new List<ItemValueDto>
        {
            new ItemValueDto { ItemKey = "aaa", Value = "111" },
            new ItemValueDto { ItemKey = "bbb", Value = "222" },
            new ItemValueDto { ItemKey = "ccc", Value = "333" }
        };

        Assert.Null(items.GetForKey("not found"));
    }

    [Fact]
    public static void CanGetDoubleForKey()
    {
        var items = new List<ItemValueDto>
        {
            new ItemValueDto { ItemKey = "aaa", Value = "1.1" },
            new ItemValueDto { ItemKey = "bbb", Value = "2.2" },
            new ItemValueDto { ItemKey = "ccc", Value = "3.3" }
        };

        Assert.Equal(2.2, items.GetDoubleForKey("bbb"));
    }

    [Fact]
    public static void GetDoubleReturnsZeroForMissingKey()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1.1" },
            new() { ItemKey = "bbb", Value = "2.2" },
            new() { ItemKey = "ccc", Value = "3.3" }
        };

        Assert.Equal(0, items.GetDoubleForKey("????"));
    }

    [Fact]
    public static void GetDoubleReturnsZeroForString()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1.1" },
            new() { ItemKey = "bbb", Value = "not a number" },
            new() { ItemKey = "ccc", Value = "3.3" }
        };

        Assert.Equal(0, items.GetDoubleForKey("bbb"));
    }

    [Fact]
    public static void CanGetInt()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "2" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.Equal(2, items.GetNullableIntForKey("bbb"));
    }

    [Fact]
    public static void GetNullableIntReturnsNullOnMissingKey()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "2" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.Null(items.GetNullableIntForKey("????"));
    }

    [Fact]
    public static void GetNullableIntReturnsNullOnNonInt()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "invalid" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.Null(items.GetNullableIntForKey("bbb"));
    }

    [Fact]
    public static void CanGetBoolForKey()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "true" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.True(items.GetBoolForKey("bbb"));
    }

    [Fact]
    public static void GetBoolReturnsFalseOnMissingKey()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "true" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.False(items.GetBoolForKey("????"));
    }

    [Fact]
    public static void GetBoolReturnsCanReturnFalse()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "false" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.False(items.GetBoolForKey("bbb"));
    }

    [Fact]
    public static void GetBoolReturnsReturnsFalseOnNonBool()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "1" },
            new() { ItemKey = "bbb", Value = "invalid value" },
            new() { ItemKey = "ccc", Value = "3" }
        };

        Assert.False(items.GetBoolForKey("bbb"));
    }

    [Fact]
    public static void CanGetItemsWithStringValues()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = null },
            new() { ItemKey = "bbb", Value = "222" },
            new() { ItemKey = "ccc", Value = null },
            new() { ItemKey = "ddd", Value = "444" }
        };

        var hasValues = items.GetItemsWithValues().ToList();
        Assert.Equal(2, hasValues.Count);
        Assert.Contains(items[1], hasValues);
        Assert.Contains(items[3], hasValues);
    }

    [Fact]
    public static void CanGetItemsWithNonZeroValues()
    {
        var items = new List<ItemValueDto>
        {
            new() { ItemKey = "aaa", Value = "0" },
            new() { ItemKey = "bbb", Value = "2" },
            new() { ItemKey = "ccc", Value = "0.0" },
            new() { ItemKey = "ddd", Value = "4" }
        };

        var hasValues = items.GetItemsWithValues().ToList();
        Assert.Equal(2, hasValues.Count);
        Assert.Contains(items[1], hasValues);
        Assert.Contains(items[3], hasValues);
    }
}
﻿using Kwolo.MaseratiConnect.WebScraper.UrlCapture;
using System.Text.Json;

namespace Kwolo.MaseratiConnect.Tests.Helpers;

/// <summary>
/// Collection of helpers that create <see cref="CaptureBody"/> objects
/// </summary>
internal static class CaptureBodyTestHelper
{
    public static string ToJson(object o)
        => JsonSerializer.Serialize(o);

    public static CaptureBody ToBody(object o, CaptureFilters.FilterTypes t)
        => new CaptureBody(t, ToJson(o));

    public static CaptureBody ToImageBody(object o)
        => new CaptureBody(CaptureFilters.FilterTypes.Image, ToJson(o));

}
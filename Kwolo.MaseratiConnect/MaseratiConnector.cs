﻿using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper;
using Microsoft.Extensions.Logging;

namespace Kwolo.MaseratiConnect;

internal class MaseratiConnector(
        ILogger<MaseratiConnector> logger,
        ISeleniumAdapter seleniumAdapter)
    : IMaseratiConnector
{
    public Task<Car?> FetchDataAsync(ConnectionDetails connectionDetails)
    {
        logger.LogInformation("Fetching from Maserati Connect at {LoginPage}", connectionDetails.LoginUrl);

        return seleniumAdapter.CollectAsync(connectionDetails);
    }
}
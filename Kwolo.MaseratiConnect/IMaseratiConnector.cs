﻿using Kwolo.MaseratiConnect.Model;

namespace Kwolo.MaseratiConnect;

public interface IMaseratiConnector
{
    public Task<Car?> FetchDataAsync(ConnectionDetails connectionDetails);
}
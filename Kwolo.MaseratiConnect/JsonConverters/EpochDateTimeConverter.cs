﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.JsonConverters;

internal class EpochDateTimeConverter :  JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return DateTime.UnixEpoch.AddMilliseconds(reader.GetInt64());
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        writer.WriteStringValue((value - DateTime.UnixEpoch).TotalMilliseconds + "000");
    }
}
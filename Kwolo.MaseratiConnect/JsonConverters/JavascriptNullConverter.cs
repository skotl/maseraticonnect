﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.JsonConverters;

internal class JavascriptNullConverter : JsonConverter<string?>
{
    public override string? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var value = reader.GetString();

        return value is null or "null" ? null : value;
    }

    public override void Write(Utf8JsonWriter writer, string? value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value);
    }
}
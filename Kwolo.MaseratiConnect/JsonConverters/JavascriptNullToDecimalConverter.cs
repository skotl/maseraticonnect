﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.JsonConverters;

public class JavascriptNullToDecimalConverter : JsonConverter<decimal?>
{
    public override decimal? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var value = reader.GetString();

        return decimal.TryParse(value, out var dec) ? dec : null;
    }

    public override void Write(Utf8JsonWriter writer, decimal? value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(value.HasValue ? value.ToString() : null);
    }
}
﻿namespace Kwolo.MaseratiConnect.Model;

/// <summary>
/// Vehicle returned from Maserati Connect
/// </summary>
public class Car
{
    /// <summary>
    /// Vehicle Identification Number
    ///  </summary>
    public string Vin { get; init; } = null!;

    /// <summary>
    /// Last update from vehicle
    /// </summary>
    public DateTime LastVehicleUpdate { get; init; }

    /// <summary>
    /// Last update from Maserati Connect
    /// </summary>
    public DateTime TimeStamp { get; init; }

    /// <summary>
    /// Number of wheels
    /// </summary>
    public int WheelCount { get; init; }

    /// <summary>
    /// Odometer reading in miles
    /// </summary>
    public int OdometerMiles { get; init; }

    /// <summary>
    /// Set of tyre pressures
    /// </summary>
    public TyrePressures TyrePressures { get; init; } = new();

    /// <summary>
    /// Set of service items requiring attention
    /// If count is zero then none reported
    /// </summary>
    public List<ServiceItemStatus> ServiceItemStatusList { get; init; } = new();

    /// <summary>
    /// Distance in miles to next service (or null, if unknown)
    /// </summary>
    public int? DistanceToService { get; init; }

    /// <summary>
    /// Number of days to next service (or null, if unknown)
    /// </summary>
    public int? DaysToService { get; init; }

    /// <summary>
    /// True if service is needed
    /// </summary>
    public bool IsServiceNeeded { get; init; }

    /// <summary>
    /// List of images of this car
    /// </summary>
    public List<CarImage> CarImagesType { get; set; }

    /// <summary>
    /// If not null, the state of the battery
    /// </summary>
    public Battery? Battery { get; set; }

    /// <summary>
    /// If not null, the current fuel levels
    /// </summary>
    public Fuel? Fuel { get; set; }
}
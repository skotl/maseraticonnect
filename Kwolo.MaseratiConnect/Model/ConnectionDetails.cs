﻿namespace Kwolo.MaseratiConnect.Model;

// ReSharper disable PropertyCanBeMadeInitOnly.Global

/// <summary>
/// Describes the details required to log on to Maserati Connect
/// </summary>
public class ConnectionDetails
{
    /// <summary>
    /// The URL (country-specific) to get to the logon page
    /// </summary>
    public string LoginUrl { get; set; } = null!;

    /// <summary>
    /// The URL (country-specific), once logged in, to get to the dashboard
    /// </summary>
    public string DashboardUrl { get; set; } = null!;

    /// <summary>
    /// User's login ID (typically email)
    /// </summary>
    public string UserLogin { get; set; } = null!;

    /// <summary>
    /// User's login password
    /// </summary>
    public string UserPassword { get; set; } = null!;

    /// <summary>
    /// If set true then the Chromium window will be displayed which can aid in
    /// debugging.
    /// Do *not* set this true if using the code in a headless / no-display app
    /// </summary>
    public bool ShowChromiumWindow { get; set; }
}
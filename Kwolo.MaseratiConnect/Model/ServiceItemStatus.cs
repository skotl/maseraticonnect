﻿namespace Kwolo.MaseratiConnect.Model;

/// <summary>
/// A service item that needs attention
/// </summary>
public class ServiceItemStatus(string item, string value)
{
    /// <summary>
    /// The name of the item (note that this is a Maserati-internal identifier
    /// </summary>
    public string Item { get; } = item;

    /// <summary>
    /// The value of the item
    /// </summary>
    public string Value { get; } = value;

    public override string ToString()
    {
        return $"{Item}={Value}";
    }
}
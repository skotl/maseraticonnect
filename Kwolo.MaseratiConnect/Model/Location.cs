﻿namespace Kwolo.MaseratiConnect.Model;

public class Location
{
    /// <summary>
    /// Altitude in meters
    /// </summary>
    public int Altitude { get; set; }

    /// <summary>
    /// Bearing, 0-360
    /// Note that this is nat always returned
    /// </summary>
    public int Bearing { get; set; }

    /// <summary>
    /// Latitude of last known position
    /// </summary>
    public double Latitude { get; set; }

    /// <summary>
    /// Longitude of last know position
    /// </summary>
    public double Longitude { get; set; }

}
﻿namespace Kwolo.MaseratiConnect.Model;

/// <summary>
/// An image of a car
/// </summary>
public class CarImage
{
    /// <summary>
    /// URL to the image
    /// </summary>
    public string Url { get; init; } = null!;

    /// <summary>
    /// View name (proprietary to Maserati), or null if none supplied
    /// </summary>
    public string? View { get; init; }

    /// <summary>
    /// Image width, or 0 if none supplied
    /// </summary>
    public int Width { get; init; }

    /// <summary>
    /// Image height, or 0 if none supplied
    /// </summary>
    public int Height { get; init; }
}
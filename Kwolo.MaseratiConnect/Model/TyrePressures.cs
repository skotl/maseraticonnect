﻿namespace Kwolo.MaseratiConnect.Model;

/// <summary>
/// A collection of tyre pressures
/// </summary>
public class TyrePressures
{
    /// <summary>
    /// Left front tyre
    /// </summary>
    public double LeftFront { get; set; }

    /// <summary>
    /// Right front tyre
    /// </summary>
    public double RightFront { get; set; }

    /// <summary>
    /// Left rear tyre
    /// </summary>
    public double LeftRear { get; set; }

    /// <summary>
    /// Right rear tyre
    /// </summary>
    public double RightRear { get; set; }

    public override string ToString()
    {
        return $"{LeftFront}, {RightFront}, {LeftRear}, {RightRear}";
    }
}
﻿namespace Kwolo.MaseratiConnect.Model;

public class Fuel
{
    /// <summary>
    /// Distance in miles until empty (may be zero if the value cannot be read)
    /// </summary>
    public int DistanceToEmptyMiles { get; set; }

    /// <summary>
    /// Amount of fuel left, in units of <see cref="FuelAmountUnits"/>
    /// </summary>
    public decimal FuelAmount { get; set; }

    /// <summary>
    /// The units that <see cref="FuelAmount"/> refers to
    /// </summary>
    public string FuelAmountUnits { get; set; }

    /// <summary>
    /// 0-100% fuel capacity
    /// </summary>
    public int FuelPercentageFull { get; set; }

    /// <summary>
    /// True if fuel level is low
    /// </summary>
    public bool IsFuelLevelLow { get; set; }
}
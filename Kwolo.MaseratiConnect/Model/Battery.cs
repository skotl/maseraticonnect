﻿namespace Kwolo.MaseratiConnect.Model;

public class Battery
{
    /// <summary>
    /// The state of charge on the battery
    /// </summary>
    public string? StateOfCharge { get; set; }

    /// <summary>
    /// A proprietary status of the battery
    /// </summary>
    public int Status { get; set; }

    /// <summary>
    /// Last read voltage of the battery
    /// </summary>
    public decimal Voltage { get; set; }
}
﻿using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.Dto;

public class LocationDto
{
    [JsonPropertyName("altitude")]
    public int Altitude { get; set; }

    [JsonPropertyName("bearing")]
    public int Bearing { get; set; }

    [JsonPropertyName("latitude")]
    public double Latitude { get; set; }

    [JsonPropertyName("longitude")]
    public double Longitude { get; set; }
}
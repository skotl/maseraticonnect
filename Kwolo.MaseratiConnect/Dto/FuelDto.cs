﻿using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Dto;

internal class FuelDto
{

    public class FuelDetailsDistanceToEmpty
    {
        [JsonPropertyName("unit")]
        public string Unit { get; set; }

        [JsonPropertyName("value")]
        [JsonConverter(typeof(JavascriptNullToDecimalConverter))]
        public decimal? Value { get; set; }
    }

    public class FuelDetailsFuelAmount
    {
        [JsonPropertyName("unit")]
        public string Unit { get; set; } = null!;

        [JsonPropertyName("value")]
        public decimal Value { get; set; }
    }

    [JsonPropertyName("fuelAmountLevel")]
    public decimal AmountLevel { get; set; }

    [JsonPropertyName("isFuelLevelLow")]
    public bool IsFuelLevelLow { get; set; }

    [JsonPropertyName("distanceToEmpty")]
    public FuelDetailsDistanceToEmpty DistanceToEmpty { get; set; } = new ()!;

    [JsonPropertyName("fuelAmount")]
    public FuelDetailsFuelAmount FuelAmount { get; set; } = new();
}
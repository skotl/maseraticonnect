﻿using System.Text;
using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Dto;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable once ClassNeverInstantiated.Global
// Reason: Completed in JsonDeserializer
internal class ItemValueDto
{
    public string ItemKey { get; set; } = null!;

    [JsonConverter(typeof(JavascriptNullConverter))]
    public string? Value { get; set; }

    public List<ItemValueDto>? Items { get; set; }

    public override string ToString()
    {
        var sb = new StringBuilder(ItemKey);

        if (!string.IsNullOrWhiteSpace(Value))
            sb.Append($"={Value}");
        else if (Items is { Count: > 0 })
            sb.Append($" ({Items.Count}) items");
        else
            sb.Append("=<null>");

        return sb.ToString();
    }
}
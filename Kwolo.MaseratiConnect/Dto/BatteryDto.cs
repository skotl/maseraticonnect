﻿using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Dto;

internal class BatteryDto
{
    public class BatteryDetailsVoltage
    {
        [JsonPropertyName("unit")]
        public string? Unit { get; set; }

        [JsonPropertyName("value")]
        public decimal Value { get; set; }
    }


    [JsonPropertyName("batteryStateOfCharge")]
    [JsonConverter(typeof(JavascriptNullConverter))]
    public string? StateOfCharge { get; set; }

    [JsonPropertyName("batteryStatus")]
    public int Status { get; set; }

    [JsonPropertyName("batteryVoltage")]
    public BatteryDetailsVoltage Voltage { get; set; } = new();
}
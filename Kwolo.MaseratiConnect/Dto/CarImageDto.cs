﻿using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.Dto;

internal class CarImageDto
{
    public class Item
    {
        [JsonPropertyName("preciseImageURL")]
        public string ImageUrl { get; set; } = null!;
    }

    [JsonPropertyName("items")]
    public List<Item> Items { get; set; } = new();
}
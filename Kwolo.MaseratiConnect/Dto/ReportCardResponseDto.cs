﻿namespace Kwolo.MaseratiConnect.Dto;

// ReSharper disable once ClassNeverInstantiated.Global
// Reason: Completed in JsonDeserializer
internal class ReportCardResponseDto
{
    public ReportCardDto ReportCard { get; set; } = null!;
}
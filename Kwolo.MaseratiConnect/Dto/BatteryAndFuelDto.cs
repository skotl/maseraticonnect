﻿using System.Text.Json.Serialization;

namespace Kwolo.MaseratiConnect.Dto;

internal class BatteryAndFuelDto
{
    public class VehicleInfo
    {
        [JsonPropertyName("batteryInfo")]
        public BatteryDto? BatteryInfo { get; set; }

        [JsonPropertyName("fuel")]
        public FuelDto? FuelInfo { get; set; }
    }

    [JsonPropertyName("vehicleInfo")]
    public VehicleInfo Vehicle { get; set; } = new()!;
}
﻿using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.JsonConverters;

namespace Kwolo.MaseratiConnect.Dto;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable once ClassNeverInstantiated.Global
// Reason: Completed in JsonDeserializer
internal class ReportCardDto
{
    [JsonConverter(typeof(EpochDateTimeConverter))]
    [JsonPropertyName("latestVehicleUpdateTs")]
    public DateTime LastVehicleUpdate { get; set; }

    [JsonConverter(typeof(EpochDateTimeConverter))]
    [JsonPropertyName("timestamp")]
    public DateTime Fetched { get; set; }

    public string Vin { get; set; } = null!;
    public int WheelCount { get; set; }

    [JsonPropertyName("items")]
    public List<ItemValueDto> ItemsLists { get; set; } = null!;

    public override string ToString()
    {
        return $"{Vin} at {LastVehicleUpdate}, fetched at {Fetched}";
    }
}
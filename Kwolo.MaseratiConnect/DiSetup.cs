﻿using Kwolo.MaseratiConnect.WebScraper;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;
using Kwolo.MaseratiConnect.WebScraper.WebAutomation;
using Microsoft.Extensions.DependencyInjection;

namespace Kwolo.MaseratiConnect;

public static class DiSetup
{
    public static void UseMaseratiConnect(this IServiceCollection services)
    {
        services
            .AddSingleton<IDashboardLoader, DashboardLoader>()
            .AddSingleton<ISeleniumDriverConfiguration, SeleniumDriverConfiguration>()
            .AddSingleton<ILogonToWebsite, LogonToWebSite>()
            .AddSingleton<IMaseratiConnector, MaseratiConnector>()
            .AddSingleton<INetworkEventHandler, NetworkEventHandler>()
            .AddSingleton<ISeleniumAdapter, SeleniumAdapter>();
    }
}
﻿using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Converters;

internal static class OdometerConverter
{
    private const string Key = "odometer";

    public static int Convert(IEnumerable<ItemValueDto> items)
    {
        var item = items.GetForKey(Key);
        if (item == default)
            return 0;

        return double.TryParse(item.Value, out var odoKms) ? KmToMileConverter.KmToMileInt(odoKms) : 0;
    }
}
﻿namespace Kwolo.MaseratiConnect.Converters;

internal static class KmToMileConverter
{
    private const double Constant = 0.621371;

    public static int KmToMileInt(decimal? km) => km.HasValue ? KmToMileInt(km.Value) : 0;

    public static int KmToMileInt(decimal km) => (int)(km * (decimal)Constant);

    public static int KmToMileInt(double km) => (int)(km * Constant);
}
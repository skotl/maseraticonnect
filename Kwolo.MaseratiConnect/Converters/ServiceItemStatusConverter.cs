﻿using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;

namespace Kwolo.MaseratiConnect.Converters;

internal static class ServiceItemStatusConverter
{
    private const string Key = "vehicleStatus";

    public static List<ServiceItemStatus> Convert(IEnumerable<ItemValueDto> items)
    {
        var serviceItems = new List<ServiceItemStatus>();

        var dtoList = items.GetForKey(Key);
        if (dtoList is { Items: not null })
        {
            serviceItems.AddRange(
                dtoList.Items.GetItemsWithValues()
                    .Select(item => new ServiceItemStatus(item.ItemKey, item.Value!)));
        }

        return serviceItems;
    }
}
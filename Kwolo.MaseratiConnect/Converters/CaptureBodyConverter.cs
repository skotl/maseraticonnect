﻿using System.Text.Json;
using System.Text.Json.Serialization;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Converters;

internal static class CaptureBodyConverter
{
    public static IEnumerable<T?> ConvertToDto<T>(this IEnumerable<CaptureBody> captures, CaptureFilters.FilterTypes filter)
    {
        var dtoList = new List<T?>();

        var options = new JsonSerializerOptions
        {
            NumberHandling = JsonNumberHandling.AllowReadingFromString
        };

        captures.Where(c => c.FilterType == filter)
            .ToList()
            .ForEach(i => dtoList.Add(JsonSerializer.Deserialize<T>(i.Body, options)));

        return dtoList;
    }
}
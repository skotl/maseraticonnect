﻿using System.Data;
using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Converters;

internal class FuelConverter
{
    /// <summary>
    /// Given a collection of <see cref="CaptureBody"/>s, extract valid fuel objects and return zero (null) or one
    /// </summary>
    /// <param name="captures"></param>
    /// <returns>A collection of <see cref="Fuel"/> objects</returns>
    public static Fuel? Convert(IEnumerable<CaptureBody> captures)
    {
        ArgumentNullException.ThrowIfNull(captures);
        var dtoList = captures.ConvertToDto<BatteryAndFuelDto>(CaptureFilters.FilterTypes.BatteryAndFuel);

        return ExtractValidFuel(dtoList);
    }

    private static Fuel ExtractValidFuel(IEnumerable<BatteryAndFuelDto?> dtoList)
    {
        var fuels = new List<Fuel?>();

        dtoList
            .Where(d => d != null)
            .ToList()
            .ForEach(d => fuels.Add(Convert(d)));

        fuels = fuels.Where(b => b != null).ToList();

        if (fuels.Count > 1)
            throw new DataException($"Expected to get zero or one fuel objects but got {fuels.Count}");

        return fuels.FirstOrDefault(b => b != null);
    }

    private static Fuel? Convert(BatteryAndFuelDto dto)
    {
        ArgumentNullException.ThrowIfNull(dto);
        ArgumentNullException.ThrowIfNull(dto.Vehicle);

        if (dto.Vehicle.FuelInfo == default)
            return default;

        return new Fuel
        {
            DistanceToEmptyMiles = KmToMileConverter.KmToMileInt(dto.Vehicle.FuelInfo.DistanceToEmpty.Value),
            FuelAmount = dto.Vehicle.FuelInfo.FuelAmount.Value,
            FuelAmountUnits = dto.Vehicle.FuelInfo.FuelAmount.Unit,
            FuelPercentageFull = (int) dto.Vehicle.FuelInfo.AmountLevel,
            IsFuelLevelLow = dto.Vehicle.FuelInfo.IsFuelLevelLow
        };
    }

}
﻿using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;

namespace Kwolo.MaseratiConnect.Converters;

internal static class TyrePressureConverter
{
    private const string ListKey = "wheelsAndSteering";
    private const string LeftFrontKey = "tirePressureLF";
    private const string RightFrontKey = "tirePressureRF";
    private const string LeftRearKey = "tirePressureLR";
    private const string RightRearKey = "tirePressureRR";


    public static TyrePressures Convert(IEnumerable<ItemValueDto> items)
    {
        var pressures = new TyrePressures();

        var tyres = items.GetForKey(ListKey);
        if (tyres is not { Items: not null })
            return pressures;

        pressures.LeftFront = tyres.Items.GetDoubleForKey(LeftFrontKey);
        pressures.RightFront = tyres.Items.GetDoubleForKey(RightFrontKey);
        pressures.LeftRear = tyres.Items.GetDoubleForKey(LeftRearKey);
        pressures.RightRear = tyres.Items.GetDoubleForKey(RightRearKey);

        return pressures;
    }
}
﻿using System.Web;
using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Converters;

internal static class CarImageConverter
{
    /// <summary>
    /// Given a collection of <see cref="CaptureBody"/>s, extract valid image objects and return them
    /// </summary>
    /// <param name="captures"></param>
    /// <returns>Collection of <see cref="CarImage"/></returns>
    public static IEnumerable<CarImage> Convert(IEnumerable<CaptureBody> captures)
    {
        ArgumentNullException.ThrowIfNull(captures);
        var dtoList = captures.ConvertToDto<CarImageDto>(CaptureFilters.FilterTypes.Image);

        return ExtractValidImages(dtoList);
    }

    private static IEnumerable<CarImage> ExtractValidImages(IEnumerable<CarImageDto?> dtoList)
    {
        // Get all the URLS out of the DTOs
        var imageUrls = new List<string>();
        dtoList.Where(d => d is { Items.Count: > 0 })
            .Select(d => d.Items)
            .ToList()
            .ForEach(items => imageUrls.AddRange(items.Select(i => i.ImageUrl)));

        return imageUrls
            .Select(Convert)
            .Where(image => image != default)
            .ToList()!;
    }

    /// <summary>
    /// Break up a Maserati image URL into its constituent parts, either return a <see cref="CarImage"/>
    /// or return null if it was not possible to decode
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private static CarImage? Convert(string url)
    {
        var idx = url.IndexOf('?');
        var query = idx >= 0 ? url[idx..] : "";

        var image = new CarImage
        {
            Url = url,
            View = HttpUtility.ParseQueryString(query).Get("view"),
            Width = SafeParse(HttpUtility.ParseQueryString(query).Get("scaleX")),
            Height = SafeParse(HttpUtility.ParseQueryString(query).Get("scaleY"))

        };

        return string.IsNullOrWhiteSpace(image.Url) ? null : image;
    }

    private static int SafeParse(string? value)
    {
        if (value == default)
            return 0;

        return int.TryParse(value, out var i) ? i : 0;
    }
}
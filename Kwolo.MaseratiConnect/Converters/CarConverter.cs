﻿using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;

namespace Kwolo.MaseratiConnect.Converters;

internal static class CarConverter
{
    private const string DistanceToServiceKey = "distanceToService";
    private const string DaysToServiceKey = "daysToService";
    private const string IsServiceNeededKey = "isServiceNeeded";

    public static Car Convert(ReportCardDto dto)
    {
        return new Car
        {
            Vin = dto.Vin,
            LastVehicleUpdate = dto.LastVehicleUpdate,
            TimeStamp = dto.Fetched,
            WheelCount = dto.WheelCount,
            OdometerMiles = OdometerConverter.Convert(dto.ItemsLists),
            TyrePressures = TyrePressureConverter.Convert(dto.ItemsLists),
            ServiceItemStatusList = ServiceItemStatusConverter.Convert(dto.ItemsLists),
            DistanceToService = dto.ItemsLists.GetNullableIntForKey(DistanceToServiceKey),
            DaysToService = dto.ItemsLists.GetNullableIntForKey(DaysToServiceKey),
            IsServiceNeeded = dto.ItemsLists.GetBoolForKey(IsServiceNeededKey)
        };
    }
}
﻿using Kwolo.MaseratiConnect.Dto;

namespace Kwolo.MaseratiConnect.Converters;

internal static class ValueGetter
{
    public static ItemValueDto? GetForKey(this IEnumerable<ItemValueDto> items, string key)
    {
        return items.FirstOrDefault(i => i.ItemKey == key);
    }

    public static double GetDoubleForKey(this IEnumerable<ItemValueDto> items, string key)
    {
        var item = GetForKey(items, key);
        if (item == default || string.IsNullOrWhiteSpace(item.Value))
            return 0;

        return double.TryParse(item.Value, out var d) ? d : 0;
    }

    public static int? GetNullableIntForKey(this IEnumerable<ItemValueDto> items, string key)
    {
        var item = GetForKey(items, key);
        if (item == default || string.IsNullOrWhiteSpace(item.Value))
            return null;

        return int.TryParse(item.Value, out var value) ? value : null;
    }

    public static bool GetBoolForKey(this IEnumerable<ItemValueDto> items, string key)
    {
        var item = GetForKey(items, key);
        if (item == default || string.IsNullOrWhiteSpace(item.Value))
            return false;

        return bool.TryParse(item.Value, out var value) && value;
    }

    public static IEnumerable<ItemValueDto> GetItemsWithValues(this IEnumerable<ItemValueDto> items)
    {
        return items.Where(HasValue);
    }

    private static bool HasValue(this ItemValueDto item)
    {
        return !string.IsNullOrWhiteSpace(item.Value) && item.Value != "0" && item.Value != "0.0";
    }
}
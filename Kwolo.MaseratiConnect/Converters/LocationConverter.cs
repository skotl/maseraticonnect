﻿using System.Data;
using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Converters;

internal class LocationConverter
{
    /// <summary>
    /// Given a collection of <see cref="CaptureBody"/>s, extract valid location objects and return zero (null) or one
    /// </summary>
    /// <param name="captures"></param>
    /// <returns>A collection of <see cref="Location"/> objects</returns>
    public static Location? Convert(IEnumerable<CaptureBody> captures)
    {
        ArgumentNullException.ThrowIfNull(captures);
        var dtoList = captures.ConvertToDto<LocationDto>(CaptureFilters.FilterTypes.Location);

        return ExtractValidLocation(dtoList);
    }

    private static Location? ExtractValidLocation(IEnumerable<LocationDto?> dtoList)
    {
        var locations = new List<Location?>();

        dtoList
            .Where(l => l != null)
            .ToList()
            .ForEach(l => locations.Add(Convert(l)));

        locations = locations.Where(b => b != null).ToList();

        if (locations.Count > 1)
            throw new DataException($"Expected to get zero or one location objects but got {locations.Count}");

        return locations.FirstOrDefault(b => b != null);
    }

    private static Location? Convert(LocationDto dto)
    {
        ArgumentNullException.ThrowIfNull(dto);

        return new Location
        {
            Altitude = dto.Altitude,
            Bearing = dto.Bearing,
            Latitude = dto.Latitude,
            Longitude = dto.Longitude
        };
    }
}
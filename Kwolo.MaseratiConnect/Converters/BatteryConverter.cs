﻿using System.Data;
using Kwolo.MaseratiConnect.Dto;
using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper.UrlCapture;

namespace Kwolo.MaseratiConnect.Converters;

internal class BatteryConverter
{
    /// <summary>
    /// Given a collection of <see cref="CaptureBody"/>s, extract valid battery objects and return zero (null) or one
    /// </summary>
    /// <param name="captures"></param>
    /// <returns>A collection of <see cref="Battery"/> objects</returns>
    public static Battery? Convert(IEnumerable<CaptureBody> captures)
    {
        ArgumentNullException.ThrowIfNull(captures);
        var dtoList = captures.ConvertToDto<BatteryAndFuelDto>(CaptureFilters.FilterTypes.BatteryAndFuel);

        return ExtractValidBatteries(dtoList);
    }

    private static Battery? ExtractValidBatteries(IEnumerable<BatteryAndFuelDto?> dtoList)
    {
        var batteries = new List<Battery?>();

        dtoList
            .Where(d => d != null)
            .ToList()
            .ForEach(d => batteries.Add(Convert(d)));

        batteries = batteries.Where(b => b != null).ToList();

        if (batteries.Count > 1)
            throw new DataException($"Expected to get zero or one batteryInfo objects but got {batteries.Count}");

        return batteries.FirstOrDefault(b => b != null);
    }

    private static Battery? Convert(BatteryAndFuelDto dto)
    {
        ArgumentNullException.ThrowIfNull(dto);
        ArgumentNullException.ThrowIfNull(dto.Vehicle);

        if (dto.Vehicle.BatteryInfo == default)
            return default;

        return new Battery
        {
            StateOfCharge = dto.Vehicle?.BatteryInfo?.StateOfCharge,
            Status = dto.Vehicle.BatteryInfo.Status,
            Voltage = dto.Vehicle.BatteryInfo.Voltage.Value
        };
    }
}
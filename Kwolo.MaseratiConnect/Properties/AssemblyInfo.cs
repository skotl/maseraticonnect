﻿
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Kwolo.MaseratiConnect.Tests")]

namespace Kwolo.MaseratiConnect.Properties;

// ReSharper disable once UnusedType.Global
// Reason: Required for InternalsVisibleTo
public class AssemblyInfo;

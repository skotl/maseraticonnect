﻿namespace Kwolo.MaseratiConnect.WebScraper.UrlCapture;

/// <summary>
/// Types of network response that we are looking for
/// </summary>
internal static class CaptureFilters
{
    /// <summary>
    /// A type of network response
    /// </summary>
    public enum FilterTypes
    {
        VehicleStatus,
        Location,
        BatteryAndFuel,
        Image
    }

    /// <summary>
    /// List of URL to network response type matches
    /// </summary>
    public static readonly Dictionary<FilterTypes, string> FilterUrls = new ()
    {
        { FilterTypes.VehicleStatus, "/vhr"},
        { FilterTypes.Location , "/location/lastknown"},
        { FilterTypes.BatteryAndFuel , "/status"},
        { FilterTypes.Image, "/image"}
    };
}
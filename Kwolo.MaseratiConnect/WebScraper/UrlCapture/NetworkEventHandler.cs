﻿using Microsoft.Extensions.Logging;
using OpenQA.Selenium;

namespace Kwolo.MaseratiConnect.WebScraper.UrlCapture;

internal class NetworkEventHandler(ILogger<NetworkEventHandler> logger) : INetworkEventHandler
{
    private SemaphoreSlim? _firstResponseReceived;
    private object _semaphoreLock = new object();

    /// <summary>
    /// The list of network response body captures that we are looking for
    /// </summary>
    public List<CaptureBody> CaptureBodies { get; } = new();

    private bool StartedToReceiveData => CaptureBodies.Count > 0;

    public async Task PrepareMonitoringAsync()
    {
        if (_firstResponseReceived != default)
            throw new InvalidOperationException("PrepareMonitoring() called more than once");

        _firstResponseReceived = new SemaphoreSlim(1, 1);
        await _firstResponseReceived.WaitAsync();
    }

    /// <summary>
    /// Fired when we get a network response from the remote web system
    /// Check whether the request is one that we are watching for (defined in <see cref="CaptureFilters"/>) and,
    /// if it is, save it so that we can parse it later
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="eventArgs"></param>
    public void NetworkResponseReceived(object? sender, NetworkResponseReceivedEventArgs eventArgs)
    {
        if (eventArgs.ResponseStatusCode == 200 && !string.IsNullOrWhiteSpace(eventArgs.ResponseBody))
        {
            foreach (var filterUrl in CaptureFilters.FilterUrls)
            {
                if (eventArgs.ResponseUrl.EndsWith(filterUrl.Value))
                {
                    CaptureBodies.Add(new CaptureBody(filterUrl.Key, eventArgs.ResponseBody));
                    logger.LogDebug("Received network response of type {FilterKey}, with body = {Body}",
                        filterUrl.Key, eventArgs.ResponseBody);
                }
            }
        }

        SafeReleaseSemaphore();
    }

    public async Task<bool> WaitForFirstResponseToBeReceivedAsync(int timeoutSeconds)
    {
        if (_firstResponseReceived == default)
            throw new InvalidOperationException("You must call PrepareMonitoringAsync() first");

        return await _firstResponseReceived.WaitAsync(timeoutSeconds * 1000);
    }

    private void SafeReleaseSemaphore()
    {
        lock (_semaphoreLock)
        {
            if (!StartedToReceiveData || _firstResponseReceived?.CurrentCount != 0)
                return;

            logger.LogDebug("Notifying that we have received the first response");
            _firstResponseReceived.Release();
        }
    }
}
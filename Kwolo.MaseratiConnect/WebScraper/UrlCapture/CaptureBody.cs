﻿namespace Kwolo.MaseratiConnect.WebScraper.UrlCapture;

/// <summary>
/// A network response body, which should contain a JSON document for the specified type
/// </summary>
internal class CaptureBody(CaptureFilters.FilterTypes filterType, string body)
{
    /// <summary>
    /// Type of request that returned the body
    /// </summary>
    public CaptureFilters.FilterTypes FilterType { get; } = filterType;

    /// <summary>
    /// Response body that should contain a JSON document
    /// </summary>
    public string Body { get; } = body;
}
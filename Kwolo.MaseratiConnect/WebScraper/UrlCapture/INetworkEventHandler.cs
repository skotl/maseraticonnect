﻿using OpenQA.Selenium;

namespace Kwolo.MaseratiConnect.WebScraper.UrlCapture;

internal interface INetworkEventHandler
{
    /// <summary>
    /// Set of captured network response bodies that should contain JSON documents describing the
    /// entities denoted by the <see cref="CaptureFilters"/>
    /// </summary>
    List<CaptureBody> CaptureBodies { get; }

    /// <summary>
    /// Prepares the event handler to start receiving traffic
    /// </summary>
    /// <returns></returns>
    Task PrepareMonitoringAsync();

    /// <summary>
    /// Event handler to deal with a network request response
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="eventArgs"></param>
    void NetworkResponseReceived(object? sender, NetworkResponseReceivedEventArgs eventArgs);

    /// <summary>
    /// Waits until either the first network response in the filter list is received, or until the timeout period
    /// is reached
    /// </summary>
    /// <returns>true if a valid response was received, false if the request timed out</returns>
    Task<bool> WaitForFirstResponseToBeReceivedAsync(int timeoutSeconds);
}
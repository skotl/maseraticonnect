﻿using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Kwolo.MaseratiConnect.WebScraper.WebAutomation;

internal class DashboardLoader(ILogger<DashboardLoader> logger) : IDashboardLoader
{
    private const string OdometerElementClass = "odometer";

    public void Load(ChromeDriver driver, string url)
    {
        logger.LogInformation("Navigating to dashboard at {Url}", url);

        driver.Navigate().GoToUrl(url);
    }
}
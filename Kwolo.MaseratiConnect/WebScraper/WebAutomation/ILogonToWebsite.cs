﻿using OpenQA.Selenium.Chrome;

namespace Kwolo.MaseratiConnect.WebScraper.WebAutomation;

internal interface ILogonToWebsite
{
    void Logon(ChromeDriver driver, string url, string login, string password);
}
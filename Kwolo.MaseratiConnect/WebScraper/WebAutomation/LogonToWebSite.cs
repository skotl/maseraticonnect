﻿using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace Kwolo.MaseratiConnect.WebScraper.WebAutomation;

internal class LogonToWebSite(ILogger<LogonToWebSite> logger) : ILogonToWebsite
{
    private const string LoginElement = "email";
    private const string PasswordElement = "password";
    private const string VinNumberElement = "vin_number";

    public void Logon(ChromeDriver driver, string url, string login, string password)
    {
        logger.LogInformation("Navigating to {Url}", url);
        driver.Navigate().GoToUrl(url);

        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
        wait.Until(ExpectedConditions.ElementIsVisible(By.Id(LoginElement)));

        logger.LogInformation("Login element is now visible, logging in as {User}", login);

        var loginField = driver.FindElement(By.Id(LoginElement));
        var passwordField = driver.FindElement(By.Id(PasswordElement));

        loginField.SendKeys(login);
        passwordField.SendKeys(password);

        loginField.Submit();

        wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(VinNumberElement)));

        logger.LogInformation("Landing page has been loaded");
    }
}
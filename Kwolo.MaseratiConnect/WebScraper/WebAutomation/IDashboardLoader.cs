﻿using OpenQA.Selenium.Chrome;

namespace Kwolo.MaseratiConnect.WebScraper.WebAutomation;

internal interface IDashboardLoader
{
    void Load(ChromeDriver driver, string url);
}
﻿using Kwolo.MaseratiConnect.Model;

namespace Kwolo.MaseratiConnect.WebScraper;

internal interface ISeleniumAdapter
{
    Task<Car?> CollectAsync(ConnectionDetails? connectionDetails);
}
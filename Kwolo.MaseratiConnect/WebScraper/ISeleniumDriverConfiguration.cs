﻿using Kwolo.MaseratiConnect.WebScraper.UrlCapture;
using OpenQA.Selenium.Chrome;

namespace Kwolo.MaseratiConnect.WebScraper;

internal interface ISeleniumDriverConfiguration
{
    ChromeDriver? Construct(bool showChromiumWindow = false);
    Task StartListeningAsync();
    Task StopListeningAsync();
    List<CaptureBody> ApiResponseBodies { get; }
    Task WaitUntilDataCollectedAsync();
}
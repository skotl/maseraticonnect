﻿using Kwolo.MaseratiConnect.Model;
using Kwolo.MaseratiConnect.WebScraper.WebAutomation;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium.Chrome;

namespace Kwolo.MaseratiConnect.WebScraper;

internal class SeleniumAdapter(
        ILogger<SeleniumAdapter> logger,
        ISeleniumDriverConfiguration seleniumDriverConfiguration,
        ILogonToWebsite logonToWebsite,
        IDashboardLoader dashboardLoader)
    : ISeleniumAdapter
{
    private ChromeDriver? _driver;

    public async Task<Car?> CollectAsync(ConnectionDetails? connectionDetails)
    {
        ArgumentNullException.ThrowIfNull(connectionDetails);
        logger.LogInformation("Starting collection from {LoginPage}", connectionDetails?.LoginUrl);

        try
        {
            return await LogonToWebsiteAndFetchCarDetailsAsync(connectionDetails!);
        }
        catch (Exception ex)
        {
            ReportException(ex);
        }
        finally
        {
            if (_driver != default)
            {
                await seleniumDriverConfiguration.StopListeningAsync();
                _driver.Quit();
            }
        }

        return null;
    }

    private async Task<Car> LogonToWebsiteAndFetchCarDetailsAsync(ConnectionDetails connectionDetails)
    {
        _driver = seleniumDriverConfiguration.Construct(connectionDetails.ShowChromiumWindow);
        if (_driver == default)
            throw new InvalidOperationException("Chrome Driver is null");

        logonToWebsite.Logon(_driver, connectionDetails.LoginUrl, connectionDetails.UserLogin,
            connectionDetails.UserPassword);

        await seleniumDriverConfiguration.StartListeningAsync();
        dashboardLoader.Load(_driver, connectionDetails.DashboardUrl);

        await seleniumDriverConfiguration.WaitUntilDataCollectedAsync();

        var apiResponses = seleniumDriverConfiguration.ApiResponseBodies;
        if (apiResponses.Count == 0)
            throw new InvalidOperationException("Failed to retrieve data from the API");

        return null;
    }

    private void ReportException(Exception ex)
    {
        logger.LogError(ex, "Fetching data");
    }
}
﻿using Kwolo.MaseratiConnect.WebScraper.UrlCapture;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools;

namespace Kwolo.MaseratiConnect.WebScraper;

internal class SeleniumDriverConfiguration(
        ILogger<SeleniumDriverConfiguration> logger,
        INetworkEventHandler networkEventHandler)
    : ISeleniumDriverConfiguration
{
    /// <summary>
    /// The maximum time we will wait, after monitoring starts, until the first response is recognised
    /// </summary>
    private const int MaxWaitForFirstResponseSeconds = 10;

    /// <summary>
    /// After the first response has been spotted, how long we wait before assuming they have all been gathered
    /// </summary>
    private const int WaitAfterFirstResponseSeconds = 10;

    private ChromeDriver? _chromeDriver;
    private NetworkManager? _networkManager;

    public List<CaptureBody> ApiResponseBodies => networkEventHandler.CaptureBodies;

    public ChromeDriver? Construct(bool showChromiumWindow = false)
    {
        logger.LogDebug("Constructing Chromium Driver");

        var options = new ChromeOptions();
        if (!showChromiumWindow)
        {
            options.AddArgument("--no-sandbox");
            options.AddArgument("--headless=new");
            options.AddArgument("--disable-dev-shm-usage");
            options.AddArgument("--remote-debugging-port=9222");
        }

        _chromeDriver = new ChromeDriver(options);

        IDevTools devTools = _chromeDriver;
        var session = devTools.GetDevToolsSession();
        var network = session.Domains.Network;

        network.EnableNetwork();
        network.EnableNetworkCaching();

        _networkManager = new NetworkManager(_chromeDriver);
        _networkManager.NetworkResponseReceived += networkEventHandler.NetworkResponseReceived;

        _chromeDriver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 30);

        logger.LogDebug("Chromium Driver ready");
        return _chromeDriver;
    }

    public async Task StartListeningAsync()
    {
        logger.LogDebug("Starting monitoring of network responses");
        if (_networkManager != default)
        {
            await networkEventHandler.PrepareMonitoringAsync();
            await _networkManager.StartMonitoring();
        }
    }

    public async Task StopListeningAsync()
    {
        logger.LogDebug("Ending monitoring of network responses");
        if (_networkManager != default)
            await _networkManager.StopMonitoring();
    }

    public async Task WaitUntilDataCollectedAsync()
    {
        logger.LogDebug("Waiting for first network response to be captured...");
        if (!await networkEventHandler.WaitForFirstResponseToBeReceivedAsync(MaxWaitForFirstResponseSeconds))
            throw new TimeoutException("Timed out waiting for first network response");

        logger.LogDebug("Sleeping for {Sleep} seconds to capture the rest of the data", WaitAfterFirstResponseSeconds);
        await Task.Delay(WaitAfterFirstResponseSeconds * 1000);
    }
}
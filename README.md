# Maserati Connect


## Project URL

https://gitlab.com/skotl/maseraticonnect


## Background

This class library allows you to connect to the Maserati Connect web site and retrieve
details of your vehicle.

As it seems that the Fiat group have done a decent job of protecting the web APIs I
haven't been able to create a 100% API-driven solution. Instead, this project uses the
Selenium WebDriver to log on to the Maserati Connect home page and, once logged on, it
listens for a request to the API and captures the response (more details below).

This class library returns a `Car` object that contains the following details 
(if available - not all of them are, and some seem to temporarily go awol...):

 * Vehicle Identification Number ("VIN")
 * Last vehicle update time
 * Wheel count (you never know when this might be handy!)
 * Distance to next service
 * Days to next service
 * Is service due flag
 * Odometer reading (in miles)
 * Fuel status (litres left, tank capacity)
 * Battery status (charging status, current voltage)
 * Last known location
 * Tyre pressures
 * List of service items that need attention


## Nuget package details

This library is available as a Nuget package titled `Kwolo.MaseratiConnect`



## Usage

There is a sample application - `Kwolo.MaseratiConnect.TestHarness` that shows how to
use the library. It is easiest to get this working in an application that already has
a hosted service / dependency injection set up. Assuming you do then start with:

```csharp
using Kwolo.MaseratiConnect;
// ...

var builder = Host.CreateApplicationBuilder(args);
// or other host builder

// ... other services configuration

// Add Maserati Connect services into the collection
builder.Services.UseMaseratiConnect();
```

To call into the service you need to construct a 
`ConnectionDetails` and populate it as:

```csharp
class ConnectionDetails{
    public string LoginUrl { get; set; }
    public string DashboardUrl { get; set; }
    public string UserLogin { get; set; }
    public string UserPassword { get; set; }
    public bool ShowChromiumWindow { get; set; }
}
```


| Property     | Description                                                                                                                                                                       |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| LoginUrl     | The URL of your Maserati connect login page. Start at https://connect.maserati.com and navigate through until you get the URL for your country's login.<br/> Use that value here. |
| DashboardUrl | Once you have the Login URL, get logged in and find the view that shows you the dashboard (where your fuel level, tyre pressure etc. is displayed).<br />Use that URL here.       |
| UserLogin    | Your Maserati Connect login, probably your email                                                                                                                                  |
| UserPassword | Your password                                                                                                                                                                     |
| ShowChromiumWindow | Defaults to false, showing no Chromium window. Set true to help debugging.                                                                                                        |



Examples of URLs for the UK would be:

`LoginUrl="https://connect.maserati.com/gb/en/login"`

`DashboardUrl="https://connect.maserati.com/gb/en/vehicle-services"`


Note the `"/gb/en"` for UK/Great Britain in English. Don't try to guess the correct
settings for your region - login and find the appropriate URLs with your own credentials.



Once you have these values, plug them into your `ConnectionDetails`
object and pass that to the `MaseratiConnector.FetchData()`
method:

```csharp
var car = _maseratiConnector.FetchData(options);
```

The resultant `Car` object should contain the data scraped from the website.


## How it works

Although the class library _requires_ screen scraping to get logged in and handle the
authentication workflow with the AWS services behind the scenes, it does *not* use 
screen scraping to get data.


Instead, it sets up a network response listener inside the Selenium driver and waits 
for specific network API calls to known endpoints.
You can can see this in action in your web browser if you turn on developer tools and
navigate to the Network tab and check for network responses.

Helpfully, the requests to these endpoints returns data in a JSON structure, so we 
just need to hoover that up into the DTOs and then convert them into the `Car` object 
and its strongly-typed children.


## Building, tests and enhancements

The class library is currently built against .net 8
with C# 12. Tests are in place for the type and value
converters.

Enhancements and PRs are welcome.



## Version history

Note that the version number in the following table represents the nuget package release number


| Version | Date | Changes |
| -- | -- | -- |
| 1.0.0 | 21 Nov 2023 | Initial release, supports VIN, wheel count, service details, odometer, tyre pressures |
| 1.1.0 | 23 Nov 2023 | Added fuel, battery and location details |


